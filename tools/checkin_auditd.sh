#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright (c) 2023, 2024
# Copyright (c) Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright (c) ndaal, Germany
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x x86 architecture; macOS Sonoma x86 architecture
# Tested on: Debian 12.x x86 architecture; macOS Sonoma x86 architecture
# 

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

if [[ "$(uname)" == "Darwin" ]]; then
  HOMEDIR="Users"
  readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
  HOMEDIR="home"
  readonly HOMEDIR
else
  echo "Unsupported operating system: $(uname)"
  exit 1
fi

# Under Linux you use `home` under macOS `Users`
echo "Home directory: ${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
echo "User script: ${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "%b\n" "\nCurrent date: ${DIRDATE}"

# Define constants
VERSION="0.5.2"
readonly VERSION

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME

SCRIPT_PATH="$(realpath "$(dirname "${0}")")"
readonly SCRIPT_PATH

SCRIPT_PATH_WITH_NAME="${SCRIPT_PATH}/${SCRIPT_NAME}"
readonly SCRIPT_PATH_WITH_NAME

DIRECTORY="${SCRIPT_PATH}/tools"
printf "Info: ${DIRECTORY}"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/repos/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

n="ndaal_customer_eswe_verkehr_feinkonzept"
echo "${n}"

Function_Git_Push_Repos () {
    echo " "
    echo "Git Push to Repos"
    echo " "
    git add --verbose -A || true
    git commit -m "update auditd best practices" || true
    git push origin HEAD:master || true
    git push --verbose --force || true
    git push origin HEAD:main || true
    git push --verbose --force || true
}

Function_Create_Default_Information_for_Repos () {
    echo "update structure.txt"
    tree > ./"structure.txt" || exit
    cat ./"structure.txt" || exit
    echo "update content_summary.txt"
    tokei ---sort code > "content_summary.txt" || exit
    cat ./"content_summary.txt" || exit
    #echo "update content_long_list.txt"
    #tokei ---sort code --files > "content_long_list.txt" || exit
    #cat ./"content_long_list.txt" || exit
}

xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_auditd_public/ndaal" || true
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_auditd_public/dataset" || true

xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_public_auditd/ndaal" || true
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_public_auditd/dataset" || true

echo "doing some stuff for ${DESTINATION}${n}/documentation/"
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_log/auditd_snippets_and_information.rst to ${DESTINATION}${n}/documentation/source/_log/" || exit
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_log/auditd_snippets_and_information.rst" "${DESTINATION}${n}/documentation/source/_log/" || exit
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules to ${DESTINATION}${n}/documentation/source/_auditd/_audit.rules/" || exit
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${DESTINATION}${n}/documentation/source/_auditd/_audit.rules/" || exit

cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${EXECUTIONPATH}ndaal_auditd_public/tools"
cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${EXECUTIONPATH}ndaal_public_auditd/tools"
cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${DESTINATION}${n}/tools"

cd "${EXECUTIONPATH}ndaal_auditd_public/" || exit
echo "${EXECUTIONPATH}ndaal_auditd_public/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${EXECUTIONPATH}ndaal_public_auditd/" || exit
echo "${EXECUTIONPATH}ndaal_public_auditd/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${DESTINATION}${n}/" || exit
echo "${DESTINATION}${n}/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${EXECUTIONPATH}ndaal_ml_infra_deploy/" || exit
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/"

#Function_Create_Default_Information_for_Repos
#Function_Git_Push_Repos

script_name1="$(basename "${0}")"
echo "script_name1: ${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
echo "script_path1: ${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
