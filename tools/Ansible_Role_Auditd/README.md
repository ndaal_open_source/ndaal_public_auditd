# Ansible Role for Auditd

This role will install and configure [Auditd](https://linux.die.net/man/8/auditd) on Debian, SUSE, and RHEL machines. Additionally, it will also configure the latest auditd rules defined [here](https://gitlab.com/ndaal_open_source/ndaal_public_auditd/-/raw/main/dataset/audit_best_practices.rules).

## Auditd (Audit Daemon)

Auditd is a Linux-based software utility that provides auditing and monitoring capabilities for system activities. It logs events like file access, process execution, user login/logout, and configuration changes. Custom rules can alse be defined for monitoring and recording logs to aid in security, compliance, and incident investigation.

## Audit Rules

The latest best-practice audit rules from ndaal are fetched by this role from [here](https://gitlab.com/ndaal_open_source/ndaal_public_auditd/-/raw/main/dataset/audit_best_practices.rules). 

However, the reference variable `auditd_rules` can be changed in the `/var/main.yml` file to reference to the rules file of choice.

## Example Playbook

For default behaviour of role (i.e. to install and configure auditd with the latest rules), use it as follows in ansible playbooks:

```yaml
- hosts: linux
  become: true
  become_method: sudo
  roles:
    - Ansible_Role_Auditd
```

## Requirements

This role has no requirements and/or dependencies.

## Roadmap

This ansible role works on debian 11.x distributions, sles 15.x distributions and rhel 8.x distributions. It can be easily expanded to other distributions in future.

Molecule testing is also provided for privileged debian containers.

## Contributions

Any contribution to improve the role is welcome.

- [Raphael Habereder](Raphael.Habereder@bamf.bund.de)
- [Pierre Gronau](pierre.gronau@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne
- [Ayesha Shafqat](ayesha.shafqat@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne
