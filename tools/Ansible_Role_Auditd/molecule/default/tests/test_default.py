#!/usr/bin/env python3
"""Test whether Auditd was successfully installed and configured."""
import os
import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_auditd_is_installed(host):
    """Test if Auditd is installed."""
    auditd_package_name = _get_auditd_package_name(host.system_info.distribution)
    auditd_package = host.package(auditd_package_name)
    assert auditd_package.is_installed


def test_auditd_rules_configured(host):
    """Test if Auditd rules file exists and test specific pattern for Debian."""
    audit_rules_file = host.file("/etc/audit/audit.rules")
    assert audit_rules_file.exists

    if host.system_info.distribution in ["debian"]:
        assert audit_rules_file.contains("--backlog_wait_time 60000")

    if host.system_info.distribution in ["redhat", "centos", "oracle"]:
        assert audit_rules_file.contains("-a always,exit -F arch=b32 -F exe=/lib64/ld-linux-x86-64.so.2 -k T1005_Data_From_Local_System_audit_log")
        assert audit_rules_file.contains("-a always,exit -F arch=b64 -F exe=/lib64/ld-linux-x86-64.so.2 -k T1005_Data_From_Local_System_audit_log")

    if host.system_info.distribution in ["opensuse", "suse"]:
        assert audit_rules_file.contains("-a exit,always -F arch=b32 -S splice -F a0=0x3 -F a2=0x5 -F a3=0x0 -F -k dirtypipe_CVE-2022-084")
        assert audit_rules_file.contains("-a exit,always -F arch=b64 -S splice -F a0=0x3 -F a2=0x5 -F a3=0x0 -F -k dirtypipe_CVE-2022-084")

def _get_auditd_package_name(host_distro):
    if host_distro in ["debian"]:
        return "auditd"
    elif host_distro in ["redhat", "centos", "oracle"]:
        return "audit"
    elif host_distro in ["opensuse", "suse"]:
        return "audit"
    else:
        raise ValueError(f"Auditd package name not defined for distribution: {host_distro}")
